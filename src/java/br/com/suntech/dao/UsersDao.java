package br.com.suntech.dao;

import br.com.suntech.entidade.Users;
import java.io.Serializable;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

public class UsersDao extends DaoImp<Users> implements UserDao,  Serializable{

    @Override
    public Users pesquisaEntidadeId(Long id, Session session) throws HibernateException {
        return (Users) session.get(Users.class, id);
    }

    @Override
    public List<Users> findAll(Session session) throws HibernateException {
        return session.createQuery("from Users").list();
    }

    @Override
    public List<Users> searchByUsernameNameEmail(String username, String name, String email, Session session) throws HibernateException {
        Query consulta = session.createQuery("from Users where username like :username and name like :name and  email like :email ");
        consulta.setParameter("username", "%" + username + "%");
        consulta.setParameter("name", "%" + name + "%");
        consulta.setParameter("email", "%" + email + "%");
        return consulta.list();
    }
   
}
