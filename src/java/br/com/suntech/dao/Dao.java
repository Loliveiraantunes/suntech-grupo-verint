package br.com.suntech.dao;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;

public interface Dao<T> {

    void salvarOuAlterar(T entidade, Session session) throws HibernateException;

    void remover(T entidade, Session session) throws HibernateException;

    T pesquisaEntidadeId(Long id, Session session) throws HibernateException;

    List<T> findAll(Session session) throws HibernateException;

    List<T> searchByUsernameNameEmail(String Username, String name, String email, Session session) throws HibernateException;

}
