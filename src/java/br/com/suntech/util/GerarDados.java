package br.com.suntech.util;

import br.com.suntech.dao.UsersDao;
import br.com.suntech.entidade.Users;
import java.util.Date;
import org.hibernate.Session;

public class GerarDados {

    private Users users;
    private UsersDao dao;
    private Session session;

    public GerarDados() {
        session = HibernateUtil.abreSessao();
        dao = new UsersDao();

        users = new Users("Alec", "123", true, new Date(), "Alec", "Does", "alec@doe.com", "12345-6789");
        dao.salvarOuAlterar(users, session);
        users = new Users("Rob", "123", true, new Date(), "Robert", "Jhon", "rob@jhon.com", "12345-6789");
        dao.salvarOuAlterar(users, session);
        users = new Users("May", "123", false, new Date(), "Maya", "White", "may@white.com", "12345-6789");
        dao.salvarOuAlterar(users, session);
        users = new Users("Luk", "123", false, new Date(), "Luke", "Willis", "luk@willis.com", "12345-6789");
        dao.salvarOuAlterar(users, session);
        users = new Users("Tom", "123", false, new Date(), "Thommas", "Alexandre", "tom@alexandre.com", "12345-6789");
        dao.salvarOuAlterar(users, session);
        users = new Users("Bob", "123", true, new Date(), "Bob", "Wuu", "bob@wuu.com", "12345-6789");
        dao.salvarOuAlterar(users, session);

    }

}
