package br.com.suntech.util;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class GeradorTabela {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("SuntechPU");

        GerarDados gd = new GerarDados();
        emf.close();
    }
}
