package br.com.suntech.controler;

import br.com.suntech.dao.UserDao;
import br.com.suntech.dao.UsersDao;
import br.com.suntech.entidade.Users;
import br.com.suntech.util.HibernateUtil;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;

@WebServlet(name = "usersControler", urlPatterns = {"/app"})
public class UsersControler extends HttpServlet {

    private UserDao dao;
    private List<Users> users;
    private Session session;
    private HttpServletRequest request;
    private HttpServletResponse response;
    private RequestDispatcher requestDispatcher;

    public UsersControler() {
        abreSessao();
        dao = new UsersDao();
       
       
    }

    private void abreSessao() {
        if (session == null) {
            session = HibernateUtil.abreSessao();
        } else if (!session.isOpen()) {
            session = HibernateUtil.abreSessao();
        }
    }
    protected void processRequest()
            throws ServletException, IOException {
    
                findElementsFiltred();
     
        requestDispatcher.forward(request, response);
    }

    private void findElementsFiltred() {
        requestDispatcher = request.getRequestDispatcher("index.jsp");
        String name = request.getParameter("name");
        String username = request.getParameter("username");
        String email = request.getParameter("email");
        users = dao.searchByUsernameNameEmail(username, name, email, session);
        request.setAttribute("users", users);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.request = request;
        this.response = response;
        processRequest();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.request = request;
        this.response = response;
        processRequest();
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
