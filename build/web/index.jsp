<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<?xml version='1.0' encoding='UTF-8' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:h="http://xmlns.jcp.org/jsf/html">
    <h:head>
        <title>Suntech Users</title>
        <link rel="stylesheet" href="assets/css/style.css"/>
        <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico">
            <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
                    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
                   

                    </h:head>
                    <h:body>
                        
                        <div class="top">
                            <p>Desafio Técnico Listagem de Usuários </br>- Leonardo Oliveira Antunes</p>
                        </div>   

                        <div class="container">
                            <div align="center">
                                <img src="assets/img/marca_cores_-_300x110.png"></img>
                            </div>
                            <h2>Lista de Usuários:</h2>
                            <form id="form" name="form" class="filtro" action="app" method="post">
                                <div class="item-form">
                                    <input type="text" name="username" placeholder="Filtrar por Username..." maxlength="100" >
                                </div>
                                <div class="item-form">
                                    <input type="text" name="name" placeholder="Filtrar por Nome..." maxlength="100" >
                                </div>
                                <div class="item-form">
                                    <input type="text" name="email" placeholder="Filtrar por Email..." maxlength="200" >
                                </div>
                                <div class="item-form btn">
                                    <input type="submit" value="Pesquisar">
                                </div>
                            </form>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Username</th>
                                        <th>password</th>
                                        <th>is_enabled</th>
                                        <th>Register_date</th>
                                        <th>Name</th>
                                        <th>Surname</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:if test="${empty users}">
                                        <tr>
                                            <td>-- --</td>
                                            <td>-- --</td>
                                            <td>-- --</td>
                                            <td>-- --</td>
                                            <td>-- --</td>
                                            <td>-- --</td>
                                            <td>-- --</td>
                                            <td>-- --</td>
                                            <td>-- --</td>
                                        </tr>
                                    </c:if>  

                                    <c:if test="${!empty users}">
                                        <c:forEach items="${users}" var="u">
                                            <tr>
                                                <td>${u.id}</td>
                                                <td>${u.username}</td>
                                                <td>${u.password}</td>
                                                <td>${u.is_enabled}</td>
                                                <td>${u.register_date}</td>
                                                <td>${u.name}</td>
                                                <td>${u.surname}</td>
                                                <td>${u.email}</td>
                                                <td>${u.phone}</td>
                                            </tr>
                                        </c:forEach>
                                    </c:if>  
                                </tbody>
                            </table>
                        </div>
                         <script>
                            function submit(){
                               if(window.location.pathname === "/Suntech_Teste/")
                                document.getElementById("form").submit();
                            }
                            submit();
                        </script>
                    </h:body>
                    </html>

